.PHONY=clean dockerize

CODEGEN=*_pb2*.py

all: $(CODEGEN) 

clean:
	rm $(CODEGEN)

dockerize: $(CODEGEN)
	docker build -t luisbelloch/acme .

test:
	gitlab-runner exec docker test

$(CODEGEN):
	python -m grpc_tools.protoc -I./protos --python_out=. --grpc_python_out=. ./protos/*.proto

