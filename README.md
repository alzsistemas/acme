# Acme

## Getting Started

```
$ virtualenv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ make
```

And from here you can run invoices service and identifier service:

```
$ python identifier.py
$ python invoice.py
```

Tests can be run as usual using `pytest`.


## Building and running Docker images locally

```
$ docker build -t acme .
$ docker run -ti acme python identifier.py
$ docker run -ti acme python invoice.py
```

Notice there's a docker compose file ready to be used.

